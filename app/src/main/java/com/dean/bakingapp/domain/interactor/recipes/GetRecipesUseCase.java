package com.dean.bakingapp.domain.interactor.recipes;

import com.dean.bakingapp.domain.interactor.type.SingleUseCase;
import com.dean.bakingapp.domain.model.Recipe;
import com.dean.bakingapp.domain.repository.RecipesRepository;

import java.util.List;

import io.reactivex.Single;

public final class GetRecipesUseCase implements SingleUseCase<List<Recipe>> {

    private final RecipesRepository recipesRepository;

    public GetRecipesUseCase(RecipesRepository recipesRepository) {
        this.recipesRepository = recipesRepository;
    }

    @Override
    public Single<List<Recipe>> execute() {
        return recipesRepository.getRecipes();
    }
}
