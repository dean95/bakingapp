package com.dean.bakingapp.domain.repository;

import com.dean.bakingapp.domain.model.Recipe;

import java.util.List;

import io.reactivex.Single;

public interface RecipesRepository {

    Single<List<Recipe>> getRecipes();
}
