package com.dean.bakingapp.util;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public final class ImageLoaderImpl implements ImageLoader {

    @Override
    public void loadImage(String url, ImageView target) {
        Picasso.get()
                .load(url)
                .into(target);
    }
}
