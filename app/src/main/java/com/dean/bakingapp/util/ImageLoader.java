package com.dean.bakingapp.util;

import android.widget.ImageView;

public interface ImageLoader {

    void loadImage(String url, ImageView target);
}
