package com.dean.bakingapp.data.recipe;

import com.dean.bakingapp.data.recipe.network.RecipesService;
import com.dean.bakingapp.domain.model.Recipe;
import com.dean.bakingapp.domain.repository.RecipesRepository;

import java.util.List;

import io.reactivex.Single;

public final class RecipesRepositoryImpl implements RecipesRepository {

    private final RecipesService recipesService;

    public RecipesRepositoryImpl(final RecipesService recipesService) {
        this.recipesService = recipesService;
    }

    @Override
    public Single<List<Recipe>> getRecipes() {
        return recipesService.fetchRecipes();
    }
}
