package com.dean.bakingapp.data.recipe.converter;

import com.dean.bakingapp.data.recipe.network.model.ApiIngredient;
import com.dean.bakingapp.data.recipe.network.model.ApiRecipe;
import com.dean.bakingapp.data.recipe.network.model.ApiStep;
import com.dean.bakingapp.domain.model.Ingredient;
import com.dean.bakingapp.domain.model.Recipe;
import com.dean.bakingapp.domain.model.Step;

import java.util.ArrayList;
import java.util.List;

public final class RecipeModelConverterImpl implements RecipeModelConverter {

    @Override
    public List<Recipe> apiToDomain(final List<ApiRecipe> recipes) {
        final List<Recipe> recipeList = new ArrayList<>(recipes.size());

        for (final ApiRecipe apiRecipe : recipes) {
            final Recipe recipe = new Recipe(apiRecipe.getId(), apiRecipe.getName(),
                    apiIngredientsToDomain(apiRecipe.getIngredients()),
                    apiStepsToDomain(apiRecipe.getSteps()),
                    apiRecipe.getServings(), apiRecipe.getImage());

            recipeList.add(recipe);
        }

        return recipeList;
    }

    private List<Ingredient> apiIngredientsToDomain(final List<ApiIngredient> ingredients) {
        final List<Ingredient> ingredientList = new ArrayList<>(ingredients.size());

        for (final ApiIngredient apiIngredient : ingredients) {
            final Ingredient ingredient = new Ingredient(apiIngredient.getQuantity(),
                    apiIngredient.getMeasure(), apiIngredient.getIngredient());
            ingredientList.add(ingredient);
        }

        return ingredientList;
    }

    private List<Step> apiStepsToDomain(final List<ApiStep> steps) {
        final List<Step> stepList = new ArrayList<>(steps.size());

        for (final ApiStep apiStep : steps) {
            final Step step = new Step(apiStep.getId(), apiStep.getShortDescription(), apiStep.getDescription(),
                    apiStep.getVideoUrl(), apiStep.getThumbnailUrl());
            stepList.add(step);
        }

        return stepList;
    }
}
