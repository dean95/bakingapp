package com.dean.bakingapp.data.recipe.network.client;

import com.dean.bakingapp.data.recipe.network.model.ApiRecipe;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface RecipesApi {

    @GET("topher/2017/May/59121517_baking/baking.json")
    Single<List<ApiRecipe>> fetchRecipes();
}
