package com.dean.bakingapp.data.recipe.converter;

import com.dean.bakingapp.data.recipe.network.model.ApiRecipe;
import com.dean.bakingapp.domain.model.Recipe;

import java.util.List;

public interface RecipeModelConverter {

    List<Recipe> apiToDomain(List<ApiRecipe> recipes);
}
