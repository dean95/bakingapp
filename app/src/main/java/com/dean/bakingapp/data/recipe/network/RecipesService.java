package com.dean.bakingapp.data.recipe.network;

import com.dean.bakingapp.domain.model.Recipe;

import java.util.List;

import io.reactivex.Single;

public interface RecipesService {

    Single<List<Recipe>> fetchRecipes();
}
