package com.dean.bakingapp.data.recipe.network;

import com.dean.bakingapp.data.recipe.converter.RecipeModelConverter;
import com.dean.bakingapp.data.recipe.network.client.RecipesApi;
import com.dean.bakingapp.domain.model.Recipe;

import java.util.List;

import io.reactivex.Single;

public final class RecipesServiceImpl implements RecipesService {

    private final RecipesApi api;
    private final RecipeModelConverter converter;

    public RecipesServiceImpl(RecipesApi api, RecipeModelConverter converter) {
        this.api = api;
        this.converter = converter;
    }

    @Override
    public Single<List<Recipe>> fetchRecipes() {
        return api.fetchRecipes()
                .map(converter::apiToDomain);
    }
}
