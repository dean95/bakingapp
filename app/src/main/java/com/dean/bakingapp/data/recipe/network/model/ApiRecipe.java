package com.dean.bakingapp.data.recipe.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ApiRecipe {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("ingredients")
    private List<ApiIngredient> ingredients;

    @SerializedName("steps")
    private List<ApiStep> steps;

    @SerializedName("servings")
    private int servings;

    @SerializedName("image")
    private String image;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<ApiIngredient> getIngredients() {
        return ingredients;
    }

    public List<ApiStep> getSteps() {
        return steps;
    }

    public int getServings() {
        return servings;
    }

    public String getImage() {
        return image;
    }
}
