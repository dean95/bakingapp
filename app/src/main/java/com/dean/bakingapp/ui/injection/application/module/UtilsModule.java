package com.dean.bakingapp.ui.injection.application.module;

import com.dean.bakingapp.util.ImageLoader;
import com.dean.bakingapp.util.ImageLoaderImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class UtilsModule {

    @Provides
    @Singleton
    ImageLoader provideImageLoader() {
        return new ImageLoaderImpl();
    }
}
