package com.dean.bakingapp.ui.base;

public interface ScopedPresenter {

    void start();

    void activate();

    void deactivate();

    void destroy();
}
