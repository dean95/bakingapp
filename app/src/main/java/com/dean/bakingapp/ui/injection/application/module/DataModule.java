package com.dean.bakingapp.ui.injection.application.module;

import com.dean.bakingapp.data.recipe.RecipesRepositoryImpl;
import com.dean.bakingapp.data.recipe.network.RecipesService;
import com.dean.bakingapp.domain.repository.RecipesRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class DataModule {

    @Provides
    @Singleton
    RecipesRepository provideRecipesRepository(final RecipesService recipesService) {
        return new RecipesRepositoryImpl(recipesService);
    }
}
