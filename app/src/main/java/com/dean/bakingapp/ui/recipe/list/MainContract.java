package com.dean.bakingapp.ui.recipe.list;

import com.dean.bakingapp.ui.base.BaseView;
import com.dean.bakingapp.ui.base.ScopedPresenter;
import com.dean.bakingapp.ui.model.RecipeViewModel;

import java.util.List;

public interface MainContract {

    interface Presenter extends ScopedPresenter {

        void getRecipes();
    }

    interface View extends BaseView {

        void renderRecipes(List<RecipeViewModel> recipeViewModels);
    }
}
