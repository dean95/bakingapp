package com.dean.bakingapp.ui.injection.application.module;

import com.dean.bakingapp.domain.interactor.recipes.GetRecipesUseCase;
import com.dean.bakingapp.domain.repository.RecipesRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class UseCaseModule {

    @Provides
    @Singleton
    GetRecipesUseCase provideGetRecipesUseCase(final RecipesRepository recipesRepository) {
        return new GetRecipesUseCase(recipesRepository);
    }
}
