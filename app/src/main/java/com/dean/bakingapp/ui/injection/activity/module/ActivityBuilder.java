package com.dean.bakingapp.ui.injection.activity.module;

import com.dean.bakingapp.ui.injection.activity.ActivityScope;
import com.dean.bakingapp.ui.recipe.list.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    @ActivityScope
    abstract MainActivity bindMainActivity();
}
