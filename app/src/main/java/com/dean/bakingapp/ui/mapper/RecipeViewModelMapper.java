package com.dean.bakingapp.ui.mapper;

import com.dean.bakingapp.domain.model.Recipe;
import com.dean.bakingapp.ui.model.RecipeViewModel;

import java.util.List;

public interface RecipeViewModelMapper {

    List<RecipeViewModel> domainRecipesToViewModel(List<Recipe> recipes);
}
