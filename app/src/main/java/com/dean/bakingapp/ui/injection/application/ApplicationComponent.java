package com.dean.bakingapp.ui.injection.application;

import android.app.Application;

import com.dean.bakingapp.ui.injection.activity.module.ActivityBuilder;
import com.dean.bakingapp.ui.injection.application.module.ApplicationModule;
import com.dean.bakingapp.ui.injection.application.module.DataModule;
import com.dean.bakingapp.ui.injection.application.module.MappersModule;
import com.dean.bakingapp.ui.injection.application.module.ServiceModule;
import com.dean.bakingapp.ui.injection.application.module.ThreadingModule;
import com.dean.bakingapp.ui.injection.application.module.UseCaseModule;
import com.dean.bakingapp.ui.injection.application.module.UtilsModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(
        modules = {
                AndroidInjectionModule.class,
                ApplicationModule.class,
                DataModule.class,
                MappersModule.class,
                ServiceModule.class,
                UseCaseModule.class,
                ThreadingModule.class,
                UtilsModule.class,
                ActivityBuilder.class,
        }
)
public interface ApplicationComponent extends ApplicationComponentInjects {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        ApplicationComponent build();
    }
}
