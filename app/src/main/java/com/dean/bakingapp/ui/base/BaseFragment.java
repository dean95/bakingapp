package com.dean.bakingapp.ui.base;

import dagger.android.DaggerFragment;

public abstract class BaseFragment extends DaggerFragment {

    public void onPreDestroy() {
        getPresenter().destroy();
    }

    public abstract ScopedPresenter getPresenter();
}
