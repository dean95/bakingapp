package com.dean.bakingapp.ui.injection.application.module;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.dean.bakingapp.ui.injection.application.ForApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class ApplicationModule {

    @Provides
    @Singleton
    @ForApplication
    Context provideContext(final Application application) {
        return application;
    }

    @Provides
    @Singleton
    Resources provideResources(final @ForApplication Context context) {
        return context.getResources();
    }
}
