package com.dean.bakingapp.ui.base;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import dagger.android.DaggerActivity;

public abstract class BaseActivity extends DaggerActivity implements BaseView {

    @Inject
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPresenter().activate();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPresenter().deactivate();
    }

    @Override
    protected void onDestroy() {
        if (isFinishing()) {
            getPresenter().destroy();
            clearFragments(fragmentManager);
        }
        super.onDestroy();
    }

    private void clearFragments(final FragmentManager fragmentManager) {
        final List<Fragment> fragments = fragmentManager.getFragments();

        for (final Fragment fragment : fragments) {
            if (fragment instanceof BaseFragment) {
                ((BaseFragment) fragment).onPreDestroy();
            }
        }
    }

    protected abstract ScopedPresenter getPresenter();
}
