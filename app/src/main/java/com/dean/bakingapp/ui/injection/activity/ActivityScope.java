package com.dean.bakingapp.ui.injection.activity;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}
