package com.dean.bakingapp.ui.model;

public final class RecipeViewModel {

    private final int id;
    private final String name;
    private final String image;

    public RecipeViewModel(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
