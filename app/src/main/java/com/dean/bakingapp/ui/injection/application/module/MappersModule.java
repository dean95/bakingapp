package com.dean.bakingapp.ui.injection.application.module;

import com.dean.bakingapp.data.recipe.converter.RecipeModelConverter;
import com.dean.bakingapp.data.recipe.converter.RecipeModelConverterImpl;
import com.dean.bakingapp.ui.mapper.RecipeViewModelMapper;
import com.dean.bakingapp.ui.mapper.RecipeViewModelMapperImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class MappersModule {

    @Provides
    @Singleton
    RecipeModelConverter provideRecipeModelConverter() {
        return new RecipeModelConverterImpl();
    }

    @Provides
    @Singleton
    RecipeViewModelMapper provideRecipeViewModelMapper() {
        return new RecipeViewModelMapperImpl();
    }
}
