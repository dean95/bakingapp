package com.dean.bakingapp.ui.recipe.list;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.widget.Toolbar;

import com.dean.bakingapp.R;
import com.dean.bakingapp.ui.base.BaseActivity;
import com.dean.bakingapp.ui.base.ScopedPresenter;
import com.dean.bakingapp.ui.model.RecipeViewModel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends BaseActivity implements MainContract.View {

    @Inject
    MainPresenter presenter;

    @Inject
    MainAdapter mainAdapter;

    @Inject
    Resources resources;

    @BindView(R.id.rv_main_recipes_container)
    RecyclerView recipesRecyclerView;

    @BindView(R.id.toolbar_main)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    @Override
    public void renderRecipes(List<RecipeViewModel> recipeViewModels) {
        mainAdapter.onRecipesUpdate(recipeViewModels);
    }

    @Override
    public ScopedPresenter getPresenter() {
        return presenter;
    }

    private void init() {

        ButterKnife.bind(this);
        setupToolbar();
        initializeRecyclerView();

        presenter.getRecipes();
    }

    private void setupToolbar() {
        toolbar.setTitle(R.string.main_toolbar_title);
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white));
        setActionBar(toolbar);
    }

    private void initializeRecyclerView() {
        if (recipesRecyclerView.getAdapter() == null) {
            recipesRecyclerView.setAdapter(mainAdapter);
        }

        mainAdapter.onItemClick()
                .subscribe(this::onRecipeSelected);
        recipesRecyclerView.setLayoutManager(new GridLayoutManager(null, numberOfColumns(),
                LinearLayoutManager.VERTICAL, false));
        recipesRecyclerView.setHasFixedSize(true);
    }

    private void onRecipeSelected(final RecipeViewModel recipeViewModel) {
        //TODO
    }

    private int numberOfColumns() {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        final int widthDivider = 400;
        final int width = displayMetrics.widthPixels;
        final int nColumns = width / widthDivider;

        if (nColumns < 2) {
            return 2;
        }

        return nColumns;
    }
}
