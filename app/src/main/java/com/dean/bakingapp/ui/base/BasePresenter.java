package com.dean.bakingapp.ui.base;

import com.dean.bakingapp.ui.injection.application.module.ThreadingModule;

import java.lang.ref.WeakReference;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<View extends BaseView> implements ScopedPresenter {

    @Inject
    @Named(ThreadingModule.BACKGROUND_SCHEDULER)
    protected Scheduler backgroundScheduler;

    @Inject
    @Named(ThreadingModule.MAIN_SCHEDULER)
    protected Scheduler mainScheduler;

    protected CompositeDisposable disposables = new CompositeDisposable();

    private WeakReference<View> viewReference;

    public BasePresenter(final View view) {
        viewReference = new WeakReference<>(view);
    }


    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {
        disposables.clear();
    }

    @Override
    public void start() {

    }

    @Override
    public void destroy() {

    }

    protected void addDisposable(final Disposable disposable) {
        if (disposables == null && disposable.isDisposed()) {
            disposables = new CompositeDisposable();
        }
        disposables.add(disposable);
    }

    protected View getView() {
        return viewReference.get();
    }
}
