package com.dean.bakingapp.ui.recipe.list;

import com.dean.bakingapp.domain.interactor.recipes.GetRecipesUseCase;
import com.dean.bakingapp.ui.base.BasePresenter;
import com.dean.bakingapp.ui.injection.activity.ActivityScope;
import com.dean.bakingapp.ui.mapper.RecipeViewModelMapper;
import com.dean.bakingapp.ui.model.RecipeViewModel;

import java.util.List;

import javax.inject.Inject;

@ActivityScope
public final class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.Presenter {

    @Inject
    GetRecipesUseCase getRecipesUseCase;

    @Inject
    RecipeViewModelMapper recipeViewModelMapper;

    @Inject
    public MainPresenter(final MainContract.View view) {
        super(view);
    }

    @Override
    public void getRecipes() {
        addDisposable(
                getRecipesUseCase.execute()
                        .subscribeOn(backgroundScheduler)
                        .observeOn(mainScheduler)
                        .map(recipeViewModelMapper::domainRecipesToViewModel)
                        .subscribe(this::recipesFetchedSuccessfully)
        );
    }

    private void recipesFetchedSuccessfully(final List<RecipeViewModel> recipeViewModels) {
        getView().renderRecipes(recipeViewModels);
    }
}
