package com.dean.bakingapp.ui.injection.application.module;

import com.dean.bakingapp.data.recipe.converter.RecipeModelConverter;
import com.dean.bakingapp.data.recipe.network.RecipesService;
import com.dean.bakingapp.data.recipe.network.RecipesServiceImpl;
import com.dean.bakingapp.data.recipe.network.client.RecipesApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public final class ServiceModule {

    private static final String BASE_URL = "https://d17h27t6h515a5.cloudfront.net/";

    @Provides
    @Singleton
    Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    RecipesService provideRecipesService(final RecipesApi recipesApi, final RecipeModelConverter recipeModelConverter) {
        return new RecipesServiceImpl(recipesApi, recipeModelConverter);
    }

    @Provides
    @Singleton
    RecipesApi provideRecipesApi(final Retrofit retrofit) {
        return retrofit.create(RecipesApi.class);
    }
}
