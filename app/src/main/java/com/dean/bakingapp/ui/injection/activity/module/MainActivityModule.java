package com.dean.bakingapp.ui.injection.activity.module;

import android.app.FragmentManager;

import com.dean.bakingapp.ui.injection.activity.ActivityScope;
import com.dean.bakingapp.ui.recipe.list.MainActivity;
import com.dean.bakingapp.ui.recipe.list.MainContract;

import dagger.Module;
import dagger.Provides;

@Module
public final class MainActivityModule {

    @Provides
    @ActivityScope
    MainContract.View provideMainView(final MainActivity mainActivity) {
        return mainActivity;
    }

    @Provides
    @ActivityScope
    FragmentManager provideSupportFragmentManager(final MainActivity mainActivity) {
        return mainActivity.getFragmentManager();
    }
}
