package com.dean.bakingapp.ui.recipe.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dean.bakingapp.R;
import com.dean.bakingapp.ui.injection.activity.ActivityScope;
import com.dean.bakingapp.ui.model.RecipeViewModel;
import com.dean.bakingapp.util.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

@ActivityScope
public final class MainAdapter extends RecyclerView.Adapter<MainAdapter.RecipeViewHolder> {

    public static final long CLICK_THROTTLE_WINDOW_MILLIS = 300L;

    private final Subject<RecipeViewModel> onItemClickSubject = BehaviorSubject.create();
    private final ImageLoader imageLoader;

    private List<RecipeViewModel> recipeViewModels = new ArrayList<>();

    @Inject
    public MainAdapter(final ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(final @NonNull ViewGroup parent, final int viewType) {
        final View itemVIew = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_item_view, parent, false);
        return new RecipeViewHolder(itemVIew, onItemClickSubject, imageLoader);
    }

    @Override
    public void onBindViewHolder(final @NonNull RecipeViewHolder holder, final int position) {
        holder.setItem(recipeViewModels.get(position));
    }

    @Override
    public int getItemCount() {
        return recipeViewModels.size();
    }

    public void onRecipesUpdate(final List<RecipeViewModel> recipeViewModels) {
        this.recipeViewModels = recipeViewModels;
        notifyDataSetChanged();
    }

    public Observable<RecipeViewModel> onItemClick() {
        return onItemClickSubject.throttleFirst(CLICK_THROTTLE_WINDOW_MILLIS, TimeUnit.MILLISECONDS);
    }

    static final class RecipeViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_main_recipe_image)
        ImageView recipeImage;

        @BindView(R.id.tv_main_recipe_name)
        TextView recipeName;

        private final Subject<RecipeViewModel> onRecipeClickSubject;
        private final ImageLoader imageLoader;

        private RecipeViewModel recipeViewModel;

        public RecipeViewHolder(final View itemView, final Subject<RecipeViewModel> onRecipeClickSubject,
                                final ImageLoader imageLoader) {
            super(itemView);
            this.onRecipeClickSubject = onRecipeClickSubject;
            this.imageLoader = imageLoader;
            ButterKnife.bind(this, itemView);
        }

        public void setItem(final RecipeViewModel recipeViewModel) {
            this.recipeViewModel = recipeViewModel;
            imageLoader.loadImage(recipeViewModel.getImage(), recipeImage);
            recipeName.setText(recipeViewModel.getName());
        }

        @OnClick(R.id.cardview_main_recipe_container)
        void onRecipeClick() {
            onRecipeClickSubject.onNext(recipeViewModel);
        }
    }
}
