package com.dean.bakingapp.ui.injection.application;

public interface ApplicationComponentInjects {

    void inject(BakingApplication bakingApplication);
}
