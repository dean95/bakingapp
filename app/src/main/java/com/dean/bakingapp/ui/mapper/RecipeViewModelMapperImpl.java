package com.dean.bakingapp.ui.mapper;

import com.dean.bakingapp.domain.model.Recipe;
import com.dean.bakingapp.ui.model.RecipeViewModel;

import java.util.ArrayList;
import java.util.List;

public final class RecipeViewModelMapperImpl implements RecipeViewModelMapper {

    private final String PLACEHOLDER_URL =
            "http://www.manuelvillacorta.com/wp-content/themes/WBR/images/recipe-placeholder.jpg";

    @Override
    public List<RecipeViewModel> domainRecipesToViewModel(final List<Recipe> recipes) {
        final List<RecipeViewModel> recipeViewModels = new ArrayList<>(recipes.size());

        for (final Recipe recipe : recipes) {
            final String imageUrl = recipe.getImage().isEmpty() ? PLACEHOLDER_URL : recipe.getImage();
            final RecipeViewModel recipeViewModel = new RecipeViewModel(recipe.getId(), recipe.getName(),
                    imageUrl);
            recipeViewModels.add(recipeViewModel);
        }

        return recipeViewModels;
    }
}
